'use strict';
var bcrypt = require('bcryptjs');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users',[
      {
        username: 'nedzad',
        password: bcrypt.hashSync('test123', 8),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'ivan',
        password: bcrypt.hashSync('test123', 8),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'sinisa',
        password: bcrypt.hashSync('test123', 8),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'sasa',
        password: bcrypt.hashSync('test123', 8),
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
