import chai from 'chai';
import chaiHttp from 'chai-http';

chai.should();
chai.use(chaiHttp);

import app from '../index';
import models from '../models';

const fakeUsers = [
  {
    username: 'nedzadk',
    password: 'test123'
  }, {
    username: 'dzevadk',
    password: 'test123'
  }, {
    username: 'esadk',
    password: 'test123'
  }
];

let loginToken = '';
let user1 = {};
let user2 = {};
let user3 = {};

before(done => {
  models.sequelize.query("SET FOREIGN_KEY_CHECKS = 0")
  .then(function(){
    return models.sequelize.sync({force: true});
  }).then(function(){
    return models.sequelize.query("SET FOREIGN_KEY_CHECKS = 1");
  }).then(() => {
    return models.User.create(fakeUsers[0]);
  })
  .then(() => {
    return models.User.create(fakeUsers[2]);
  })
  .then(() => {
    return models.User.findOne({ where: { username: fakeUsers[0].username }})
  })
  .then((user) => {
    user1 = user;
  })
  .then(() => {
    return models.User.findOne({ where: { username: fakeUsers[2].username }})
  })
  .then((user) => {
    user2 = user;
  })
  .done(()=> {
    done()
  });
});



describe('/signup', () => {

  it('Should register user', (done) => {
    chai
      .request(app)
      .post('/v1/signup')
      .send(fakeUsers[1])
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  it('Should not register user with duplicated username', (done) => {
    chai
      .request(app)
      .post('/v1/signup')
      .send(fakeUsers[1])
      .end((err, res) => {
        res.should.have.status(422);
        done();
      });
  });

});

describe('/login', () => {

  it('Should login user', (done) => {
    chai
      .request(app)
      .post('/v1/login')
      .send(fakeUsers[1])
      .end((err, res) => {
        loginToken = res.body.token;
        res.should.have.status(200);
        res.body.should.have.property('token');
        done();
      });
  });

  it('Should return invalid password', (done) => {
    chai
      .request(app)
      .post('/v1/login')
      .send({username: fakeUsers[1].username, password: 'dummy'})
      .end((err, res) => {
        res.body.should.have.property('message');
        done();
      });
  });

});

describe('/me', () => {

  it('Should fail as unauthorized', (done) => {
    chai
      .request(app)
      .get('/v1/me')
      .set('Authorization', 'Bearer fakeToken')
      .end((err, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Should return user details', (done) => {
    chai
      .request(app)
      .get('/v1/me')
      .set('Authorization', loginToken)
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;
        user3 = res.body.userInfo;
        done();
      });
  });

  it('Should fail to update password - 401', (done) => {
    chai
      .request(app)
      .put('/v1/me/update-password')
      .set('Authorization', 'Bearer fakeToken')
      .send({password: 'newpassword'})
      .end((err, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Should update password', (done) => {
    chai
      .request(app)
      .put('/v1/me/update-password')
      .set('Authorization', loginToken)
      .send({password: 'newpassword'})
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

});

describe('/user/:id/like', () => {
  
  it('Should like user', (done) => {
    chai
      .request(app)
      .post(`/v1/user/${user1.id}/like`)
      .set('Authorization', loginToken)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  it('Self like is not allowed', (done) => {
    chai
      .request(app)
      .post(`/v1/user/${user3.id}/like`)
      .set('Authorization', loginToken)
      .end((err, res) => {
        res.should.have.status(422);
        done();
      });
  });

  it('Should not like user - 401', (done) => {
    chai
      .request(app)
      .post(`/v1/user/${user2.id}/like`)
      .set('Authorization', 'Bearer fakeToken')
      .end((err, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Should not like user two times', (done) => {
    chai
      .request(app)
      .post(`/v1/user/${user1.id}/like`)
      .set('Authorization', loginToken)
      .end((err, res) => {
        res.should.have.status(422);
        done();
      });
  });
});

describe('/user/:id', () => {

  it('Should get user data', (done) => {
  chai
    .request(app)
    .get(`/v1/user/${user1.id}`)
    .end((err, res) => {
      res.should.have.status(200);
      done();
    });
  });

});

describe('/most-liked', () => {

  it('Should return users ordered by number of likes', (done) => {
  chai
    .request(app)
    .get(`/v1/most-liked`)
    .end((err, res) => {
      res.should.have.status(200);
      res.should.be.json
      done();
    });
  });
  
});

describe('/user/:id/unlike', () => {

  it('Should unlike user', (done) => {
  chai
    .request(app)
    .delete(`/v1/user/${user1.id}/unlike`)
    .set('Authorization', loginToken)
    .end((err, res) => {
      res.should.have.status(200);
      done();
    });
  });

  it('Should not unlike user - 401', (done) => {
    chai
      .request(app)
      .delete(`/v1/user/${user2.id}/unlike`)
      .set('Authorization', 'Bearer fakeToken')
      .end((err, res) => {
        res.should.have.status(401);
        done();
      });
  });
});

