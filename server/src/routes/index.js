import express from 'express';
import passport from 'passport';

import * as user from '../controllers/user';
import * as me from '../controllers/me';

import '../libs/passport';

const router = express.Router();

router.post('/login', user.login);
router.post('/signup', user.signup);
router.post('/user/:id/like', passport.authenticate('jwt', {session: false}), user.like);
router.delete('/user/:id/unlike', passport.authenticate('jwt', {session: false}), user.unlike);

router.get('/user/:id', user.getUser);
router.get('/most-liked', user.getMostLiked);
router.get('/me', passport.authenticate('jwt', {session: false}), me.data);
router.put('/me/update-password', passport.authenticate('jwt', {session: false}), me.updatePassword);

export default router;
