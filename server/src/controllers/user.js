import bcrypt from 'bcryptjs';
import { Sequelize, ValidationError } from 'sequelize';

import { signToken } from '../helpers/token';
import * as db from '../models/index';

export async function login(req, res) {
    try {
    let user = await db.User.findOne({where: { username: req.body.username }});
    if (user) {
      let match = await bcrypt.compare(req.body.password, user.password);
      if (match) {
        return res.status(200).json({ token: signToken(user) });
      } else {
        throw 'Wrong password';
      }
    } 
    throw 'User not found';
  } catch(err) {
    return res.status(422).json({ error: true, message: err });
  } 
}

export async function signup(req, res) {
  try {
    let user = await db.User.create(req.body);
    if (user) {
      return res.status(200).json({ auth: true, token: signToken(user) });
    } 
  } catch(err) {
    if (err instanceof ValidationError) {
        return res.status(422).json({ error: true, message: 'Username already taken' });
     } else {
      return res.status(422).json({ error: true, message: err });   
    }
  }
}

export async function like(req, res) {
  try {
    if (req.user.id != req.params.id) {
      let likeExist = await db.UserLikes.findOne({where :{userId: req.user.id, likedUserId: req.params.id}});
      if (!likeExist) {
        await db.UserLikes.create({userId: req.user.id, likedUserId: req.params.id});
        return res.status(200).json({status: 'done'});
      }
      throw 'Already liked'
    } else {
      throw 'Self like not allowed'
    }
  } catch (err) {
    res.status(422).json({ error: true, message: err });
  }
}

export async function unlike(req, res) {
  try {
    let like = await db.UserLikes.findOne({where :{userId: req.user.id, likedUserId: req.params.id}});
    if (like) {
      await like.destroy();
      return res.status(200).json({status: 'done'});
    } 
    throw 'Nothing to like';
  } catch (err) {
    res.status(422).json({ error: true, message: err });
  }
}

export async function getUser(req, res) {
  try {
    let userInfo = await db.User.findOne({
        attributes: ['id', 'username'],
        where : {id: req.params.id}, 
        include: [{ 
          model: db.UserLikes, 
          attributes: ['userId'],
          required: false 
        }]
      });
    if (userInfo) {
      let likeNumber = userInfo.UserLikes.length;
      return res.status(200).json({userInfo, likeNumber});
    } 
    throw 'User not found';
  } catch (err) {
    res.status(422).json({ error: true, message: err});
  }
}

export async function getMostLiked(req, res, next) {
  try {
    let mostLiked = await db.User.findAll({
      attributes: { 
        include: [[Sequelize.fn("COUNT", Sequelize.col("TotalUserLikes.userId")), "likeCount"]],
        exclude: ['password', 'createdAt', 'updatedAt'],
      },
      include: [{
          model: db.UserLikes, 
          as: 'TotalUserLikes',
          attributes: []
      }],
      group: ['User.id'],
      order: [[Sequelize.literal('likeCount'), 'DESC']]

    });

    if (mostLiked) {
      return res.status(200).json(mostLiked); 
    }
    return res.status(200).json([])
  } catch (err) {
    next(err);
  }
}
