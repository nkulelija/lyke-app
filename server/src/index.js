import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import passport from 'passport';

import config from './config/app';
import logger from './libs/logger';
import setCors from './libs/cors';
import routes from './routes/';

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(morgan('dev'));

// CORS setup 
app.use(setCors);

// Load routes 
app.use('/v1', routes);

app.use((req, res, next) => {
  let err = new Error('Not found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  if (config.env != 'test') {
    if (err) {
      logger(err, 'error');
      if (config.env != 'production') {
          delete err.stack;
      }
      res.status(err.statusCode || 500).send({error: true, message: err});
      return next(err);
    }
  } else {
    return next(err);
  }
});

app.listen(config.serverPort, () => {
 logger(`LykeServer is running on port ${config.serverPort}`); 
})

module.exports = app;