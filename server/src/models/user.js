import bcrypt from 'bcryptjs';
export default function (sequelize, DataTypes) {
  var User = sequelize.define('User', {
    username: { 
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
      hooks: {
        beforeCreate: async (user) => {
          let hashedPassword = await bcrypt.hashSync(user.password, 8);
          user.password = hashedPassword;
        },
        beforeUpdate: async (user) => {
          if (user.changed('password')) {
            let hashedPassword = await bcrypt.hashSync(user.password, 8);
            user.password = hashedPassword;
          }
        }
      }
    });
  User.associate = function (models) {
    User.hasMany(models.UserLikes, {
      as: 'TotalUserLikes',
      foreignKey: 'likedUserId'
    });

    User.hasMany(models.UserLikes, {
      foreignKey: 'userId'
    });
  };
  return User;
}
