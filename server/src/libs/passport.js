import passport from 'passport';
import passportJWT from 'passport-jwt';
import { User } from '../models/index';
import config from '../config/app';

const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : config.secret
    },
    async (jwtPayload, cb) => {
        try {
            let user = await User.findOne({where: { id: jwtPayload.user_id }});
            cb(null, user);
        } catch (err) {
            cb(err, null);
        }
    }
));