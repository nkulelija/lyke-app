import winston from 'winston';

export default function logger(log, type = 'info') {
  switch(type) {
    case 'error':
      winston.error(log);
      break;
    case 'danger':
      winston.danger(log);
      break;
    default:
      winston.info(log);
      break;
  } 

}