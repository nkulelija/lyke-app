import jwt from 'jsonwebtoken';
import config from '../config/app';

export function signToken(user) {
  return `Bearer ${jwt.sign({ user_id: user.id, username: user.username }, config.secret, {
    expiresIn: 86400
  })}`
}