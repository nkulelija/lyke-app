import { spawn } from 'child-process-promise';
const spawnOptions = { stdio: 'inherit' };
(async () => {
try {
    // Migrate the DB
    const createDb = await spawn('./node_modules/.bin/sequelize', ['db:drop'], spawnOptions);
    console.log('*************************');
    console.log('Database deleted');
  } catch (err) {
    // Oh no!
    console.log('*************************');
    console.log('Deleting database failed. Error:', err.message);
    process.exit(1);
  }
process.exit(0);
})();