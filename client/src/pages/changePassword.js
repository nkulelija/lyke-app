import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { updatePassword } from '../actions/me';
import { logoutUser } from '../actions/auth';

class ChangePassword extends Component {
  handleFormSubmit = newPassword => {
    this.props.updatePassword(newPassword);
  }

  renderError = () => {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Error!</strong>
          <br />
          {this.props.errorMessage}
        </div>
      );
    }
    return <div />;
  };

  renderPasswordField = field => {
    const {
      meta: { touched, error },
    } = field;
    return (
      <div>
        <input
          {...field.input}
          type="password"
          className="form-control"
          minLength="5"
          maxLength="15"
          placeholder={field.placeholder}
          required
        />
        <div className="text-help">{touched ? error : ''}</div>
      </div>
    );
  };

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="starter-template">
        <form
          className="form-signin"
          onSubmit={handleSubmit(this.handleFormSubmit)}
        >
          <h1 className="h3 mb-3 font-weight-normal">Change Password</h1>
          <Field
            name="oldPassword"
            component={this.renderPasswordField}
            placeholder="Current Password"
          />
          <br />
          <Field
            name="newPassword"
            component={this.renderPasswordField}
            placeholder="New Password"
          />
          <Field
            name="newPasswordConfirm"
            component={this.renderPasswordField}
            placeholder="New Password (confirm)"
          />
          <br />
          {this.renderError()}
          <button className="btn btn-lg btn-primary btn-block" type="submit">
            Change
          </button>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.me.error,
  };
}

function validate(values) {
  const errors = {};

  if (values.newPassword !== values.newPasswordConfirm) {
    errors.newPasswordConfirm =
      'The password confirmation is not the same as the password entered';
  }

  return errors;
}

export default reduxForm({
  validate,
  form: 'ChangePasswordForm',
})(
  connect(
    mapStateToProps,
    { updatePassword, logoutUser },
  )(ChangePassword),
);
