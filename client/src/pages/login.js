import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { loginUser } from '../actions/auth';

function mapStateToProps(state) {
  return { errorMessage: state.auth.error };
}

class Login extends Component {
  handleFormSubmit = (username, password) => {
    this.props.loginUser(username, password)
  }

  renderError() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Error!</strong>
          <br/>
          {this.props.errorMessage}
        </div>
      );
    }
    return null;
  }

  renderUsernameField = field => {
    return (
      <input
        {...field.input}
        type="text"
        className="form-control"
        placeholder="Username"
        required
      />
    );
  }

  renderPasswordField = field => {
    return (
      <input
        {...field.input}
        type="password"
        className="form-control"
        placeholder="Password"
        required
      />
    );
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="starter-template">
        <form
          className="form-signin"
          onSubmit={handleSubmit(this.handleFormSubmit)}
        >
          <h1 className="h3 mb-3 font-weight-normal">Log in</h1>
          <Field name="username" component={this.renderUsernameField} />
          <Field name="password" component={this.renderPasswordField} />
          <br/>
          {this.renderError()}
          <button className="btn btn-lg btn-primary btn-block" type="submit">
            Login
          </button>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'LoginForm',
})(
  connect(
    mapStateToProps,
    { loginUser },
  )(Login),
);
