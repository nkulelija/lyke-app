import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Navbar from '../../components/navbar';
import Home from '../../pages/home';
import Login from '../../pages/login';
import Register from '../../pages/register';
import ChangePassword from '../../pages/changePassword';

import { getMe } from '../../actions/me';
import { checkExistingToken } from '../../actions/auth';

class App extends Component {
  componentDidMount() {
    this.props.checkExistingToken();
    this.props.getMe();
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Route component={Navbar}/>
          <main className="container">
            <Switch>
              <Route path="/login" component={Login}/>
              <Route path="/signup" component={Register}/>
              <Route path="/change" component={ChangePassword}/>
              <Route path="/" component={Home}/>
            </Switch>
          </main>
        </div>
      </BrowserRouter>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: state.users,
    authenticated: state.auth.authenticated,
  };
}

export default connect(
  mapStateToProps,
  { getMe, checkExistingToken },
)(App);
