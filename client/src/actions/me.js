import axios from 'axios';

import { API_URL } from '../config/api';
import { GET, UPDATE_PASSWORD_ERROR } from '../constants/actionTypes';

export async function getMe() {
  const response = await axios.get(`${API_URL}/me`);
  return {
    type: GET,
    payload: response.data.userInfo,
  };
}

export const updatePassword = (password) => async(dispatch) => {
  try {
    await axios.put(`${API_URL}/me/update-password`, { password: password.newPassword });
    dispatch(await getMe());
    window.location = '/login';
  } catch(error) {
    dispatch(UPDATE_PASSWORD_ERROR);
  } 
}
