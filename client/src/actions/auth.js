import axios from 'axios';
import { API_URL } from '../config/api';
import { AUTH_USER, AUTH_LOGOUT, AUTH_ERROR } from '../constants/actionTypes';
import { getMe } from './me';

export function authUser(token) {
  const AUTH_TOKEN = token;
  localStorage.setItem('token', AUTH_TOKEN);
  axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
}

export const loginUser = ({ username, password }) => async(dispatch) => {
  try {
    let response = await axios.post(`${API_URL}/login`, { username, password });
    authUser(response.data.token);
    getMe();
    dispatch({ type: AUTH_USER });
    window.location = '/';
  } catch(error) {
    dispatch({ type: AUTH_ERROR, payload: error.response.data.message });
  }
}

export const signupUser = ({ username, password }) => async(dispatch) => {
  try {
    let response = await axios.post(`${API_URL}/signup`, { username, password });
    dispatch({ type: AUTH_USER });
    getMe();
    authUser(response.data.token);
    window.location = '/';
  } catch(error) {
    dispatch({ type: AUTH_ERROR, payload: error.response.data.message });
  }
}

export function logoutUser() {
  localStorage.removeItem('token');
  delete axios.defaults.headers.common.Authorization;

  return {
    type: AUTH_LOGOUT,
  };
}

export function checkExistingToken() {
  return dispatch => {
    const AUTH_TOKEN = localStorage.getItem('token');

    if (AUTH_TOKEN) {
      axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
      dispatch({ type: AUTH_USER });
    }
  };
}
