import axios from 'axios';

import { API_URL } from '../config/api';

import {
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  DISABLE_LIKE_BUTTON,
  ENABLE_LIKE_BUTTON,
} from '../constants/actionTypes';

export function disableLikeButton(idUser) {
  return {
    type: DISABLE_LIKE_BUTTON,
    payload: idUser,
  };
}

// We do not use this currently...
export function enableLikeButton(idUser) {
  return {
    type: ENABLE_LIKE_BUTTON,
    payload: idUser,
  };
}

export function getUser(idUser) {
  return dispatch => {
    dispatch({ type: UPDATE_USER });

    axios
      .get(`${API_URL}/user/${idUser}`)
      .then(response => {
        dispatch({
          type: UPDATE_USER_SUCCESS,
          payload: response.data,
        });
      })
      .catch(error => {
        dispatch({
          type: UPDATE_USER_FAILURE,
          payload: error,
        });
      });
  };
}

export function getUsers() {
  return dispatch => {
    dispatch({ type: FETCH_USERS });

    axios
      .get(`${API_URL}/most-liked`)
      .then(response => {
        dispatch({
          type: FETCH_USERS_SUCCESS,
          payload: response.data,
        });
      })
      .catch(error => {
        dispatch({
          type: FETCH_USERS_FAILURE,
          payload: error,
        });
      });
  };
}

export function likeUser(idUser) {
  return dispatch => {
    dispatch(disableLikeButton(idUser));
    axios
      .post(`${API_URL}/user/${idUser}/like`)
      .then(() => {
        dispatch(getUser(idUser));
      })
      .catch(error => {
        dispatch({
          type: 'GLOBAL_ERROR',
          payload: error,
        });
      });
  };
}

export function unlikeUser(idUser) {
  return dispatch => {
    dispatch(disableLikeButton(idUser));
    axios
      .delete(`${API_URL}/user/${idUser}/unlike`)
      .then(() => {
        dispatch(getUser(idUser));
      })
      .catch(error => {
        dispatch({
          type: 'GLOBAL_ERROR',
          payload: error,
        });
      });
  };
}
