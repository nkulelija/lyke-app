import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import authReducer from './auth';
import usersReducer from './users';
import meReducer from './me';

const rootReducer = combineReducers({
  form: formReducer,
  auth: authReducer,
  users: usersReducer,
  me: meReducer,
});

export default rootReducer;
