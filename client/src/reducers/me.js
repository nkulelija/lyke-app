import { GET, UPDATE_PASSWORD_ERROR } from '../constants/actionTypes';

const INIT_STATE = {
  currentUser: {},
  error: null,
};

export default function(state = INIT_STATE, action) {
  switch (action.type) {
    case GET:
      return { currentUser: action.payload, error: null };
    case UPDATE_PASSWORD_ERROR:
      return { currentUser: state.currentUser, error: action.payload };
    default:
      return state;
  }
}
