import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { logoutUser } from '../actions/auth';
import { getMe } from '../actions/me';

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    currentUser: state.me.currentUser
  };
}

class ConnectedNavbar extends Component {
  onLogOutClick() {
    this.props.logoutUser();
    this.props.history.push('/');
  }

  render() {
    const { authenticated, currentUser } = this.props;
    const menuItems = [];
    let welcomeMessage = null;

    if (authenticated) {
      menuItems.push(
        <li className="nav-item" key="1">
          <Link to={`/change`} className="nav-link">Change Pass { currentUser.username}</Link>
        </li>
      );
      menuItems.push(
        <li className="nav-item" key="2">
          <a className="nav-link"
             style={{cursor: 'pointer'}}
             onClick={this.onLogOutClick.bind(this)}>Log Out</a>
        </li>
      );
    } else {
      menuItems.push(
        <li className="nav-item" key="3">
          <Link to={`/login`} className="nav-link">Login</Link>
        </li>
      );
      menuItems.push(
        <li className="nav-item" key="4">
          <Link to={`/signup`} className="nav-link">Signup</Link>
        </li>
      );
    }

    return (
      <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <div className="container">
          <Link to={`/`} className="navbar-brand">Lyke Client - Povio App</Link>
          <button className="navbar-toggler"
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarsMain"
                  aria-controls="navbarsMain"
                  aria-expanded="false"
                  aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse"
               id="navbarsMain">
            <span className="navbar-text">
              {welcomeMessage}
            </span>
            <ul className="navbar-nav ml-auto">
              {menuItems}
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

const Navbar = connect(mapStateToProps, { logoutUser, getMe })(ConnectedNavbar);

export default Navbar;
