
## Povio Lyke Application

#### Folder structure

  

##### /client

Contains React JS application (used create-react-app for bootstraping)
Other technologies used: *Redux*, *Redux Thunk*, *Axios*

How to run application
```
yarn
yarn start
```

How to run tests
```
yarn test
```
#### /server
Contains NodeJS server application
Other technologies used: *express*, *sequelize*

**Edit database configuration and setup**

Configuration file location:
```
/server/src/config/config.json
```
Install depenendecies and setup database (after you configured config.json)
```
yarn
yarn db:setup
```
**Run server**

```
yarn dev
```

**Run tests**

Create test database (uses test config in /server/src/config/config.json)

```
yarn pre-test
```

Run tests
```
yarn test
```

Delete test database after you are done with testing

```
yarn post-test
```
**info**

By renaming package.json scripts *pre-test* and *post-test* these two would trigger automatically when *yarn test* is run, but to avoid any errors while you are testing i changed their names so they have to be run manually.

**ScreenShots**

Server tests

![](https://i.imgur.com/36UZOQd.png)

Client

![](https://i.imgur.com/Pr8poke.png)
